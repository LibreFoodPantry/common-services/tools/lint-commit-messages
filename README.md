# lint-commits-on-branch

Checks that the commit messages on the current feature branch adhere to
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

Does not run on main because it's too late to prevent bad commit messages
from reaching main if they are already on main.

This is a thin wrapper around
[commitlint](https://github.com/conventional-changelog/commitlint)

## Usage

The best way to use this is through
[Pipeline](https://gitlab.com/LibreFoodPantry/common-services/tools/pipeline).

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
