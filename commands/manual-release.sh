#!/usr/bin/env bash

LOCAL_IMAGE="lint-commit-messages"
REMOTE_IMAGE="registry.gitlab.com/librefoodpantry/common-services/tools/$LOCAL_IMAGE"

commands/build.sh
docker tag "$LOCAL_IMAGE" "$REMOTE_IMAGE:latest"
docker push "$REMOTE_IMAGE:latest"
docker tag "$LOCAL_IMAGE" "$REMOTE_IMAGE:main"
docker push "$REMOTE_IMAGE:main"
